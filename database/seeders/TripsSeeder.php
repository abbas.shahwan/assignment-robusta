<?php

namespace Database\Seeders;

use App\Models\Route;
use App\Models\Trip;
use Illuminate\Database\Seeder;

class TripsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Trip::factory()
            ->create([
                'route_id' => Route::query()->first()->id,
                'bus_id' => Route::query()->first()->id,
            ]);
    }
}
