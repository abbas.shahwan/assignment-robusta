<?php

namespace Database\Seeders;

use App\Models\Route;
use App\Models\Station;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RoutesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $routes = [
            [
                'name' => 'Cairo - Asyut',
                'stations' => ['Cairo', 'Giza', 'Faiyum', 'Beni Suef', 'Minya', 'Asyut'],
            ],
        ];

        foreach ($routes as $route) {
            /**
             * @var $createdRoute Route
             */
            $createdRoute =
                Route::query()
                ->firstOrCreate(['name' => $route['name']]);

            $stationsOrder = "'".implode('\',\'', $route['stations'])."'";

            $stations = Station::query()
                ->select(['id'])
                ->whereIn('name', $route['stations'])
                ->orderByRaw(DB::raw("FIELD(name, $stationsOrder)"))
                ->get()->toArray();

            $stationsWithPivot = [];
            foreach ($stations as $k => $station) {
                $stationsWithPivot[$station['id']] = ['order' => $k + 1];
            }

            $createdRoute->stations()->sync($stationsWithPivot);
        }
    }
}
