<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('route_has_stations', function (Blueprint $table) {
            $table->unsignedBigInteger('route_id');
            $table->unsignedBigInteger('station_id');
            $table->unsignedInteger('order');

            $table->foreign('route_id')
                ->references('id')
                ->on('routes')->onDelete('cascade');

            $table->foreign('station_id')
                ->references('id')
                ->on('stations')->onDelete('cascade');

            $table->primary(['route_id', 'station_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('route_has_stations');
    }
};
