<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookings', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('trip_id');
            $table->unsignedBigInteger('seat_id');
            $table->unsignedBigInteger('station_from');
            $table->unsignedBigInteger('station_to');
            $table->timestamp('cancelled_at')->nullable();
            $table->timestamps();

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->cascadeOnDelete();

            $table->foreign('trip_id')
                ->references('id')
                ->on('trips')
                ->cascadeOnDelete();

            $table->foreign('seat_id')
                ->references('id')
                ->on('seats')
                ->cascadeOnDelete();

            $table->foreign('station_from')
                ->references('id')
                ->on('stations')
                ->cascadeOnDelete();

            $table->foreign('station_to')
                ->references('id')
                ->on('stations')
                ->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bookings');
    }
};
