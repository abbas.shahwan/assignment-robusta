## About The Project
This is a Hiring assignment for Robusta Studio.

## Running The project
- PHP 8.1, local server, MySQL and composer i.e. "_Laravel 9 requirements + PHP 8.1 due to some dependencies requirements_"
- Clone the project
- Run `composer install` inside the project directory.
- A valid .env database configuration.
- A valid Application key `php artisan key:generate`
- Run migrations `php artisan migrate`
- Database can be filled with testing data using `php artisan db:seed` ore `--seed` flag during migration.
- Tests can be run using `./vendor/bin/phpunit` command inside the project directory.

## Project Specs
- A user can list the available seats of a given trip via a GET request to `api/bookings/bookings/seats-available` API endpoint "_a none authenticated endpoint_".
- A user can book a seat on a given trip via an authenticated post request to `api/bookings` endpoint:
  - "trip, seat, from, and to" are required parameters for a booking to be created they are corresponding to 'trip id, seat id, station_from, and station_to' columns of the bookings table.
  - A booking is allowed if the following rules are fulfilled:
    - The trip is not ended.
    - The seat doesn't have any bookings.
    - The booking route isn't the same as an already booked route for the same trip and seat.
    - The booking route is the same as an already booked route for the same seat but a different trip.
    - The booking route is not intersecting with an already booked route for the same seat and trip.
    
- Authentication is done via sending a Bearer token in the request headers.
- Auth tokens can be issued via a post request containing a valid email & password combination of a registered user.
