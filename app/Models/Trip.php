<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Trip extends Model
{
    use HasFactory;

    public function route()
    {
        return $this->belongsTo(Route::class);
    }

    public function bookings()
    {
        return $this->hasMany(Booking::class);
    }

    public function bus()
    {
        return $this->belongsTo(Bus::class);
    }

    public function users()
    {
        return $this->hasManyThrough(User::class, Booking::class);
    }

    /**
     * Check if the trip has ended
     *
     * @return bool
     */
    public function isEnded()
    {
        return ! is_null($this->ended_at);
    }
}
