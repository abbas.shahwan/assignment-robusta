<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    use HasFactory;

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function trip()
    {
        return $this->belongsTo(Trip::class);
    }

    public function seat()
    {
        return $this->belongsTo(Seat::class);
    }

    public function departure()
    {
        return $this->belongsTo(Station::class, 'station_from');
    }

    public function drop()
    {
        return $this->belongsTo(Station::class, 'station_to');
    }

    public function isCancelled()
    {
        return ! is_null($this->cancelled_at);
    }
}
