<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Route extends Model
{
    use HasFactory;

    protected $fillable = ['name'];

    public function stations()
    {
        return $this
            ->belongsToMany(Station::class, 'route_has_stations')
            ->withPivot('order')->orderBy('order');
    }

    public function trips()
    {
        return $this->hasMany(Trip::class);
    }
}
