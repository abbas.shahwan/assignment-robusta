<?php

namespace App\Http\Controllers;

use App\Http\Resources\BookingResource;
use App\Http\Resources\SeatResourceCollection;
use App\Models\Booking;
use App\Models\Trip;
use App\Models\User;
use App\Rules\BookableSeatForTrip;
use App\Rules\IsBookableTrip;
use App\Rules\ValidDepartureDropOffStationsForTrip;
use App\Rules\ValidSeatForTrip;
use App\Services\TripSeatsAvailability;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BookingsController extends Controller
{
    /**
     * Action method for bookings/available-seats endpoint.
     * Returns a list of the available seats for booking for a given
     * destination and trip.
     *
     * @param  Request  $request
     * @return mixed
     */
    public function availableSeats(Request $request)
    {
        $validated = $request->validate([
            'trip' => ['required', 'numeric', 'exists:trips,id', new IsBookableTrip()],
            'from' => ['required', 'numeric', new ValidDepartureDropOffStationsForTrip()],
            'to' => ['required', 'numeric'],
        ]);

        /**
         * @var $trip Trip
         */
        $trip = Trip::query()
            ->where('id', 1)
            ->with(['bus.seats.bookings', 'route.stations'])->first();

        $seatsAvailable = (new TripSeatsAvailability($trip, $validated['from'], $validated['to']));

        return new SeatResourceCollection($seatsAvailable->getAvailable());
    }

    /**
     * Create a new booking in the database
     *
     * @param  Request  $request
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'trip' => ['required', 'numeric', 'exists:trips,id', new IsBookableTrip()],
            'from' => ['required', 'numeric', new ValidDepartureDropOffStationsForTrip()],
            'to' => ['required', 'numeric'],
            'seat' => ['required', 'numeric', 'exists:seats,id', new ValidSeatForTrip(), new BookableSeatForTrip()],
        ]);

        $booking = new Booking();
        $booking->trip_id = $validated['trip'];
        $booking->station_from = $validated['from'];
        $booking->station_to = $validated['to'];
        $booking->seat_id = $validated['seat'];

        /**
         * @var $user User
         */
        $user = Auth::user();
        $saved = $user->bookings()->save($booking);

        if ($saved === false) {
            return response([
                'message' => __('An error occurred while trying to save your booking.'),
                'errors' => [],
            ], 500);
        }

        return new BookingResource($saved);
    }
}
