<?php

namespace App\Services;

use App\Models\Seat;
use App\Models\Trip;

class TripSeatsAvailability
{
    protected Trip $trip;

    protected array $tripStations = [];

    protected int $stationFrom;

    protected int $stationTo;

    protected array $destinationSubRoutes;

    public function __construct(Trip $trip, int $stationsFrom, int $stationTo)
    {
        $this->trip = $trip;
        $this->stationFrom = $stationsFrom;
        $this->stationTo = $stationTo;

        $this->setTripStations();
        $this->setDestinationSubRoutes();
    }

    public function getAvailable()
    {
        return
            $this->trip->bus->seats->filter(function (Seat $seat) {
                return $this->isAvailableSeat($seat);
            });
    }

    /**
     * Check if a trip seat is available for booking.
     *
     * @param  Seat  $seat
     * @return bool
     *
     * @throws \Exception
     */
    public function isAvailableSeat(Seat $seat)
    {
        if (! $seat->bookings->count()) {
            return true;
        }

        foreach ($seat->bookings as $booking) {
            if ($booking->trip_id !== $this->trip->id) {
                continue;
            }

            $bookingSubRoutes = $this->destinationToSubRoutes($booking->station_from, $booking->station_to);
            if ($this->subRoutesConflicting($bookingSubRoutes)) {
                return false;
            }
        }

        return true;
    }

    /**
     * Check if already booked sub-routes are conflicting with the requested sub-routes
     *
     * @param  array  $bookedSubRoutes
     * @return bool
     */
    protected function subRoutesConflicting(array $bookedSubRoutes)
    {
        $interSection = array_map(
            'unserialize',
            array_intersect(
                array_map('serialize', $bookedSubRoutes),
                array_map('serialize', $this->destinationSubRoutes)
            )
        );

        return count($interSection) > 0;
    }

    /**
     * Set the given destination stations
     *
     * @throws \Exception
     */
    protected function setDestinationSubRoutes()
    {
        $this->destinationSubRoutes = $this->destinationToSubRoutes($this->stationFrom, $this->stationTo);
    }

    /**
     * Set the array of ordered trip stations
     */
    protected function setTripStations()
    {
        $this->tripStations =
            $this->trip->route->stations->pluck('id')->toArray();
    }

    /**
     * Convert two destination ends to sub-routes along the trip route
     *
     * @param  int  $stationFrom
     * @param  int  $stationTo
     * @return array
     *
     * @throws \Exception
     */
    protected function destinationToSubRoutes(int $stationFrom, int $stationTo)
    {
        $stations = $this->destinationToStations($stationFrom, $stationTo);

        return $this->stationsToSubRoutes($stations);
    }

    /**
     * Get all stations between two ends on the trip route
     *
     * @param  int  $stationFrom The start station
     * @param  int  $stationTo The end station
     * @return array
     *
     * @throws \Exception
     */
    protected function destinationToStations(int $stationFrom, int $stationTo)
    {
        $fromIndex = array_search($stationFrom, $this->tripStations);

        if (false === $fromIndex) {
            throw new \Exception('The supplied station is not on the route');
        }

        $stations = [];
        for ($i = $fromIndex; $i < count($this->tripStations); $i++) {
            if ($this->tripStations[$i] == $stationTo) {
                break;
            }
            $stations[] = $this->tripStations[$i];
        }

        $stations[] = $stationTo;

        return $stations;
    }

    /**
     * Convert an array of stations IDs to an array of sub-routes
     *
     * @param  array  $stations
     * @return array
     */
    protected function stationsToSubRoutes(array $stations)
    {
        $routes = [];
        foreach ($stations as $k => $station) {
            $nextStation = $stations[$k + 1] ?? null;
            if ($nextStation) {
                $routes[] = [$station, $nextStation];
            }
        }

        return $routes;
    }
}
