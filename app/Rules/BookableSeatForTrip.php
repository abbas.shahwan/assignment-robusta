<?php

namespace App\Rules;

use App\Models\Trip;
use App\Services\TripSeatsAvailability;
use Illuminate\Contracts\Validation\DataAwareRule;
use Illuminate\Contracts\Validation\Rule;

class BookableSeatForTrip implements Rule, DataAwareRule
{
    protected $data = [];

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if (
            ! isset($this->data['trip'])
            || ! isset($this->data['from'])
            || ! isset($this->data['to'])
        ) {
            return false;
        }

        /**
         * @var $trip Trip
         */
        $trip = Trip::query()
            ->with(['bus.seats.bookings', 'route.stations'])
            ->where('id', $this->data['trip'])->first();

        if (! $trip) {
            return false;
        }

        $seatAvailability = new TripSeatsAvailability(
            $trip,
            $this->data['from'],
            $this->data['to']
        );

        $seat = $trip?->bus?->seats?->firstWhere('id', $value);

        return $seat && $seatAvailability->isAvailableSeat($seat);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('The selected seat is no longer available for booking');
    }

    public function setData($data)
    {
        $this->data = $data;

        return $this;
    }
}
