<?php

namespace App\Rules;

use App\Models\Trip;
use Illuminate\Contracts\Validation\DataAwareRule;
use Illuminate\Contracts\Validation\Rule;

class ValidDepartureDropOffStationsForTrip implements Rule, DataAwareRule
{
    protected $data = [];

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if (
            ! isset($this->data['trip'])
            || ! isset($this->data['to'])
            || ! isset($this->data['from'])
        ) {
            return false;
        }

        $trip = Trip::query()->with('route.stations')
            ->where('id', $this->data['trip'])->first();

        $tripStations = $trip?->route?->stations;
        $fromStation = $tripStations?->firstWhere('id', $value);
        $toStation = $tripStations?->firstWhere('id', $this->data['to']);

        return $fromStation && $toStation && $fromStation->pivot->order < $toStation->pivot->order;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('Invalid departure/drop-off stations combination for the selected trip');
    }

    public function setData($data)
    {
        $this->data = $data;

        return $this;
    }
}
