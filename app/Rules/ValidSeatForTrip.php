<?php

namespace App\Rules;

use App\Models\Trip;
use Illuminate\Contracts\Validation\DataAwareRule;
use Illuminate\Contracts\Validation\Rule;

class ValidSeatForTrip implements Rule, DataAwareRule
{
    protected $data = [];

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if (! isset($this->data['trip'])) {
            return false;
        }

        $trip = Trip::query()
            ->with('bus.seats')
            ->where('id', $this->data['trip'])->first();

        $seat = $trip?->bus?->seats?->firstWhere('id', $value);

        return ! is_null($seat);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('The selected seat is not valid');
    }

    public function setData($data)
    {
        $this->data = $data;

        return $this;
    }
}
