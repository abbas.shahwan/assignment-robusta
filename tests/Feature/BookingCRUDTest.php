<?php

namespace Tests\Feature;

use App\Models\Booking;
use App\Models\Trip;
use App\Models\User;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class BookingCRUDTest extends TestCase
{
    use RefreshDatabase;

    public function test_create_valid_booking()
    {
        $this->seed();
        /**
         * @var $user Authenticatable
         */
        $user = User::query()->first();
        $trip = Trip::query()->with(['bus.seats'], ['route.stations'])->first();
        $stationFrom = $trip->route->stations->first()->id;
        $stationTo = $trip->route->stations->last()->id;
        $seat = $trip->bus->seats->first()->id;

        $response = $this->actingAs($user)
            ->postJson('api/bookings', [
                'trip' => $trip->id,
                'seat' => $seat,
                'from' => $stationFrom,
                'to' => $stationTo,
            ]);
        $response->assertStatus(201);
        $this->assertSame($trip->id, $response->json()['data']['trip_id']);
        $this->assertDatabaseHas(Booking::class, [
            'trip_id' => $trip->id,
            'seat_id' => $seat,
            'station_from' => $stationFrom,
            'station_to' => $stationTo,
        ]);
    }

    public function test_booking_not_allowed_for_non_authenticated()
    {
        $this->seed();
        $trip = Trip::query()->with(['bus.seats'], ['route.stations'])->first();
        $stationFrom = $trip->route->stations->first()->id;
        $stationTo = $trip->route->stations->last()->id;
        $seat = $trip->bus->seats->first()->id;

        $response = $this
            ->postJson('api/bookings', [
                'trip' => $trip->id,
                'seat' => $seat,
                'from' => $stationFrom,
                'to' => $stationTo,
            ]);

        $response->assertStatus(401);

        $this->assertDatabaseMissing(Booking::class, [
            'trip_id' => $trip->id,
            'seat_id' => $seat,
            'station_from' => $stationFrom,
            'station_to' => $stationTo,
        ]);
    }

    public function test_trip_id_is_required_for_booking_creation()
    {
        $this->seed();
        /**
         * @var $user Authenticatable
         */
        $user = User::query()->first();
        $trip = Trip::query()->with(['bus.seats'], ['route.stations'])->first();
        $stationFrom = $trip->route->stations->first()->id;
        $stationTo = $trip->route->stations->last()->id;
        $seat = $trip->bus->seats->first()->id;

        $response = $this
            ->actingAs($user)
            ->postJson('api/bookings', [
                'seat' => $seat,
                'from' => $stationFrom,
                'to' => $stationTo,
            ]);

        $response->assertStatus(422);
        $response->assertJson(function (AssertableJson $json) {
            return $json->has('errors.trip')->etc();
        });

        $this->assertDatabaseMissing(Booking::class, [
            'seat_id' => $seat,
            'station_from' => $stationFrom,
            'station_to' => $stationTo,
        ]);
    }

    public function test_seat_id_is_required_for_booking_creation()
    {
        $this->seed();
        /**
         * @var $user Authenticatable
         */
        $user = User::query()->first();
        $trip = Trip::query()->with(['bus.seats'], ['route.stations'])->first();
        $stationFrom = $trip->route->stations->first()->id;
        $stationTo = $trip->route->stations->last()->id;

        $response = $this
            ->actingAs($user)
            ->postJson('api/bookings', [
                'trip' => $trip->id,
                'from' => $stationFrom,
                'to' => $stationTo,
            ]);

        $response->assertStatus(422);
        $response->assertJson(function (AssertableJson $json) {
            return $json->has('errors.seat')->etc();
        });
        $this->assertDatabaseMissing(Booking::class, [
            'trip_id' => $trip->id,
            'station_from' => $stationFrom,
            'station_to' => $stationTo,
        ]);
    }

    public function test_departure_station_id_is_required_for_booking_creation()
    {
        $this->seed();
        /**
         * @var $user Authenticatable
         */
        $user = User::query()->first();
        $trip = Trip::query()->with(['bus.seats'], ['route.stations'])->first();
        $stationTo = $trip->route->stations->last()->id;
        $seat = $trip->bus->seats->first()->id;

        $response = $this
            ->actingAs($user)
            ->postJson('api/bookings', [
                'trip' => $trip->id,
                'seat' => $seat,
                'to' => $stationTo,
            ], ['Accept' => 'application/json']);

        $response->assertStatus(422);
        $response->assertJson(function (AssertableJson $json) {
            return $json->has('errors.from')->etc();
        });
        $this->assertDatabaseMissing(Booking::class, [
            'trip_id' => $trip->id,
            'seat_id' => $seat,
            'station_to' => $stationTo,
        ]);
    }

    public function test_booking_not_allowed_for_the_same_trip_seat_and_route()
    {
        $this->seed();
        /**
         * @var $user Authenticatable
         */
        $user = User::query()->first();
        $trip = Trip::query()->with(['bus.seats'], ['route.stations'])->first();
        $stationFrom = $trip->route->stations->first()->id;
        $stationTo = $trip->route->stations->last()->id;
        $seat = $trip->bus->seats->first()->id;

        $userAnother = User::factory()->create();
        $booking = new Booking();
        $booking->trip_id = $trip->id;
        $booking->seat_id = $seat;
        $booking->station_from = $stationFrom;
        $booking->station_to = $stationTo;
        $userAnother->bookings()->save($booking);

        $response = $this->actingAs($user)
            ->postJson('api/bookings', [
                'trip' => $trip->id,
                'seat' => $seat,
                'from' => $stationFrom,
                'to' => $stationTo,
            ]);
        $response->assertStatus(422);
        $response->assertJson(function (AssertableJson $json) {
            $json->has('errors.seat')->etc();
        });
    }

    /**
     * Test that a booking can be done for the same seat and the same
     * destination of an already booked route as long as it is a different trip
     */
    public function test_booking_is_allowed_for_the_same_seat_and_route_but_different_trip()
    {
        $this->seed();
        /**
         * @var $user Authenticatable
         */
        $user = User::query()->first();
        $trip = Trip::query()->with(['bus.seats'], ['route.stations'])->first();
        $stationFrom = $trip->route->stations->first()->id;
        $stationTo = $trip->route->stations->last()->id;
        $seat = $trip->bus->seats->first()->id;

        /**
         * Create another trip with the same bus and route
         * Note: a bus can have multiple trips because different schedules can
         * be available
         */
        $tripAnother = Trip::factory()->create([
            'bus_id' => $trip->bus->id,
            'route_id' => $trip->route->id,
        ]);

        /**
         * Create a booking for the first trip
         */
        $userAnother = User::factory()->create();
        $booking = new Booking();
        $booking->trip_id = $trip->id;
        $booking->seat_id = $seat;
        $booking->station_from = $stationFrom;
        $booking->station_to = $stationTo;
        $userAnother->bookings()->save($booking);

        /**
         * create a booking for the other trip with the same route
         * and seat
         */
        $response = $this->actingAs($user)
            ->postJson('api/bookings', [
                'trip' => $tripAnother->id,
                'seat' => $seat,
                'from' => $stationFrom,
                'to' => $stationTo,
            ]);

        $response->assertStatus(201);

        $this->assertDatabaseHas(Booking::class, [
            'seat_id' => $seat,
            'station_from' => $stationFrom,
            'station_to' => $stationTo,
            'trip_id' => $trip->id,
        ]);

        $this->assertDatabaseHas(Booking::class, [
            'seat_id' => $seat,
            'station_from' => $stationFrom,
            'station_to' => $stationTo,
            'trip_id' => $tripAnother->id,
        ]);
    }

    /**
     * Test that more than one booking can be created for the same trip
     * and seat as long as no intersection is found between any already booked
     * route.
     */
    public function test_multiple_booking_is_allowed_for_none_conflicting_sub_route_same_trip_and_seat()
    {
        $this->seed();
        /**
         * @var $user Authenticatable
         */
        $user = User::query()->first();
        $trip = Trip::query()->with(['bus.seats'], ['route.stations'])->first();
        $stationFrom = $trip->route->stations->first()->id;
        $stationTo = $trip->route->stations()->wherePivot('order', 3)->first()->id;
        $seat = $trip->bus->seats->first()->id;

        $userAnother = User::factory()->create();
        $booking = new Booking();
        $booking->trip_id = $trip->id;
        $booking->seat_id = $seat;
        $booking->station_from = $stationFrom;
        $booking->station_to = $stationTo;
        $userAnother->bookings()->save($booking);

        $stationFrom2 = $trip->route->stations()->wherePivot('order', 4)->first()->id;
        $stationTo2 = $trip->route->stations->last()->id;

        $response = $this->actingAs($user)
            ->postJson('api/bookings', [
                'trip' => $trip->id,
                'seat' => $seat,
                'from' => $stationFrom2,
                'to' => $stationTo2,
            ]);

        $response->assertStatus(201);

        $this->assertDatabaseHas(Booking::class, [
            'seat_id' => $seat,
            'station_from' => $stationFrom,
            'station_to' => $stationTo,
            'trip_id' => $trip->id,
        ]);

        $this->assertDatabaseHas(Booking::class, [
            'seat_id' => $seat,
            'station_from' => $stationFrom2,
            'station_to' => $stationTo2,
            'trip_id' => $trip->id,
        ]);
    }

    /**
     * Test that booking the same seat for the same trip is not
     * allowed when the destination has an intersection between the
     * any already booked route on the same trip and seat.
     */
    public function test_multiple_booking_is_not_allowed_for_conflicting_sub_routes_same_trip_and_seat()
    {
        $this->seed();
        /**
         * @var $user Authenticatable
         */
        $user = User::query()->first();
        $trip = Trip::query()->with(['bus.seats'], ['route.stations'])->first();
        $stationFrom = $trip->route->stations->first()->id;
        $stationTo = $trip->route->stations()->wherePivot('order', 3)->first()->id;
        $seat = $trip->bus->seats->first()->id;

        $userAnother = User::factory()->create();
        $booking = new Booking();
        $booking->trip_id = $trip->id;
        $booking->seat_id = $seat;
        $booking->station_from = $stationFrom;
        $booking->station_to = $stationTo;
        $userAnother->bookings()->save($booking);

        $stationFrom2 = $trip->route->stations()->wherePivot('order', 2)->first()->id;
        $stationTo2 = $trip->route->stations->last()->id;

        $response = $this->actingAs($user)
            ->postJson('api/bookings', [
                'trip' => $trip->id,
                'seat' => $seat,
                'from' => $stationFrom2,
                'to' => $stationTo2,
            ]);

        $response->assertStatus(422);

        $this->assertDatabaseHas(Booking::class, [
            'seat_id' => $seat,
            'station_from' => $stationFrom,
            'station_to' => $stationTo,
            'trip_id' => $trip->id,
        ]);

        $this->assertDatabaseMissing(Booking::class, [
            'seat_id' => $seat,
            'station_from' => $stationFrom2,
            'station_to' => $stationTo2,
            'trip_id' => $trip->id,
        ]);
    }

    /**
     * Test that the departure station must come before the drop-off
     * station in the trip route.
     */
    public function test_booking_not_allowed_for_wrong_ordered_destination()
    {
        $this->seed();
        /**
         * @var $user Authenticatable
         */
        $user = User::query()->first();
        $trip = Trip::query()->with(['bus.seats'], ['route.stations'])->first();
        $stationFrom = $trip->route->stations->last()->id;
        $stationTo = $trip->route->stations->first()->id;
        $seat = $trip->bus->seats->first()->id;

        $response = $this->actingAs($user)
            ->postJson('api/bookings', [
                'trip' => $trip->id,
                'seat' => $seat,
                'from' => $stationFrom,
                'to' => $stationTo,
            ]);

        $response->assertStatus(422);

        $this->assertDatabaseMissing(Booking::class, [
            'trip_id' => $trip->id,
            'seat_id' => $seat,
            'station_from' => $stationFrom,
            'station_to' => $stationTo,
        ]);
    }
}
